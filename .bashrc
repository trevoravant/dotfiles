#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# basic
export VISUAL=vim
export EDITOR="$VISUAL"
alias ls='ls --color=auto'
set -o vi

# full prompt (username, host, directory)
function full_prompt() {

	# colors for the primary prompt (PS1)
	# note: \033 = \e
	bold_clr="\[\e[1m\]"
	u_clr="\[\e[38;2;255;95;255m\]"
	at_clr="\[\e[38;2;255;255;255m\]"
	h_clr="\[\e[38;2;210;0;210m\]"
	#W_clr="\[\e[38;2;175;175;255m\]"
	W_clr="\[\e[38;2;165;165;255m\]"
	dollar_clr="\[\e[38;2;255;255;255m\]"
	reset_clr="\[\e[m\]"

	#PS1="$bold_clr$u_clr\u$at_clr@$h_clr\h $W_clr\W$dollar_clr\$$reset_clr "
	PS1="$bold_clr$u_clr\u$at_clr@$h_clr\h $W_clr\W$reset_clr "
	#PS1="\[\033[38;2;255;95;255m\]\u $ucol \h \W\$\[\e[m\] "
}


# rainbow prompt
function rainbow_prompt() {

	bold_clr="\[\e[1m\]"
	R="\[\e[48;2;255;0;0m\]"
	O="\[\e[48;2;255;127;0m\]"
	Y="\[\e[48;2;255;255;0m\]"
	G="\[\e[48;2;0;255;0m\]"
	B="\[\e[48;2;0;0;255m\]"
	I="\[\e[48;2;75;0;130m\]"
	V="\[\e[38;2;255;95;255m\]"
	reset_clr="\[\e[m\]"

	PS1="$bold_clr$R $O $Y $G $B $I $reset_clr$bold_clr$V\W$reset_clr "
}

# partial prompt (directory only)
function partial_prompt() {

	bold_clr="\[\e[1m\]"
	pink_clr="\[\e[38;2;255;95;255m\]"
	reset_clr="\[\e[m\]"

	PS1="$bold_clr$pink_clr\W$reset_clr "
	PROMPT_COMMAND=prompt_bar
}

# draw a horizontal bar before the prompt is drawn 
function prompt_bar() {

	dark_pink_clr="\e[38;2;61;0;61m"
	if ! [[ -v prompt_entered ]]
	then
		prompt_entered=1
	else
		echo -en $dark_pink_clr
		for i in `seq 1 $COLUMNS`
		do
			printf "\u2500"
		done
		echo -en "\e[m"
	fi

}

# computer-dependent stuff
if [ "$HOSTNAME" = trevorsdesktop ]; then
	export MOZ_ENABLE_WAYLAND=1
fi

# theme
export GTK_THEME=Arc-Dark

# history
export HISTCONTROL=ignoredups
export HISTSIZE=10000
export HISTFILESIZE=10000

# python
export PYTHONPATH="${PYTHONPATH}:/home/trevor/ACC_2020_Avant:/home/trevor/affine_relu_sensitivity/:/home/trevor/pose_estimation/:/home/trevor/rotations3d/"

partial_prompt
export PATH=$HOME/bin:$PATH

#export GTK_USE_PORTAL=0
