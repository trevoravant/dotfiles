autoload -Uz compinit promptinit
compinit
promptinit

# aliases
################################################################################
alias ls='ls --color=auto'


# prompt
################################################################################
# https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html
# %F{red}  start using a different foreground color (red)
# %B       bold
# %c       trailing component of working directory 
# %f       stop using a different foreground color
PROMPT='%F{123}%B%c%b%f '


# vi mode
################################################################################
# vi mode (instead of emacs mode)
bindkey -v
# show "-- VI --" in vi mode, so I know I'm in vi mode
# it seems that I need to put this before the keybindings section
# https://unix.stackexchange.com/questions/547/make-my-zsh-prompt-show-mode-in-vi-mode
#function zle-line-init zle-keymap-select {
#    RPS1="${${KEYMAP/vicmd/-- VI --}/(main|viins)/}"
#    RPS2=$RPS1
#    zle reset-prompt
#}
#zle -N zle-line-init
#zle -N zle-keymap-select
## change cursor color to orange in vi mode
## https://unix.stackexchange.com/a/743110/294686
_reset_cursor_color() printf '\e]112\a'
zle-keymap-select() {
    if [[ $KEYMAP = vicmd ]]; then
        printf '\e]12;#ffa500\a'
    else
        _reset_cursor_color
    fi
}
zle -N zle-keymap-select
zle-line-init() zle -K viins
zle -N zle-line-init
precmd_functions+=(_reset_cursor_color)


# keybindings
###############################################################################
# make the Insert, Delete, Home, End, etc. keys work
# AND
# do a history-based autocomplete with the Up and Down keys
# https://wiki.archlinux.org/title/Zsh#Key_bindings
# https://unix.stackexchange.com/questions/97843/how-can-i-search-history-with-text-already-entered-at-the-prompt-in-zsh
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         history-beginning-search-backward
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       history-beginning-search-forward
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi


# autocompletion
################################################################################
# make 1st tab press show autocompletion options,
# and subsequent tab presses do nothing (rather than cycle through various options)
# https://zsh.sourceforge.io/Guide/zshguide06.html (6.2.1: Ambiguous completions)
unsetopt automenu


# history
################################################################################
# https://unix.stackexchange.com/a/389883/294686
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory
setopt hist_ignore_dups
