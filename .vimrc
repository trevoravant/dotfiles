" vim-plug
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/vimplugplugs')

Plug 'jpalardy/vim-slime'
Plug 'scrooloose/nerdtree'
Plug 'Valloric/YouCompleteMe'
Plug 'tpope/vim-surround'

" colors
Plug 'junegunn/seoul256.vim'
Plug 'tyrannicaltoucan/vim-deep-space'
Plug 'morhetz/gruvbox'
Plug 'catppuccin/vim'

call plug#end()


" toggle functions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! ToggleConcealLevel()
	if &conceallevel == 0
		setlocal conceallevel=2
	elseif &conceallevel == 2
		setlocal conceallevel=0
	endif
endfunction

function! ToggleColorColumn(ncols)
	if &cc == ''
		execute 'set cc=' . a:ncols
	else
		set cc=
	endif
endfunction

function! ToggleSpellCheck()
	if &spell
		setlocal nospell
	else
		setlocal spell
	endif
endfunction


" various stuff
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set encoding=utf-8
set noexpandtab
set tabstop=4
set shiftwidth=4
nnoremap <space> <nop>
set clipboard=unnamedplus " copy to both vim register and system register
if has('win32linux')
	set clipboard=unnamed
endif
set backspace=indent,eol,start " make backspace delete things in insert mode (in Arch Linux, this variable is set in /usr/share/vim/vimfiles/archlinux.vim)
set timeoutlen=500 " time to wait for key sequences
set wildmode=longest,list,full
set wildmenu
set shortmess-=S " show match numbers when doing a search
set ruler " show line numbers at the bottom right of the screen

" letter commands
nnoremap s :w<cr>
nnoremap L :bn<cr>
nnoremap <leader>L L
nnoremap H :bp<cr>
nnoremap <leader>H H
nnoremap J 20j
nnoremap K 20k
nnoremap X :bd<cr>
nnoremap <c-d> :q<cr>

" leader commands
let mapleader = "\<space>"
let maplocalleader = "\<space>"
nnoremap <leader>; ;
nnoremap <leader>ec :call system('tmux send-keys -t output C-c')<cr>
nnoremap <leader>ed
	\ :call system('tmux send-keys -t output C-d && wmctrl -c output')<cr>
"nnoremap <leader>ee :call system('create_output_terminal.sh')<cr>
nnoremap <leader>ee :terminal ++hidden ++close create_output_terminal.sh<cr>
nnoremap <leader>ef :call system(
	\ 'this_WID=$(xdotool getactivewindow) && ' .
	\ 'wmctrl -a output && ' .
	\ 'wmctrl -i -a $this_WID'
	\ )<cr><cr>
nnoremap <leader>eg :call system('wmctrl -a output')<cr>
nnoremap <leader>eq :call system('tmux send-keys -t output q Enter')<cr>
nnoremap <leader>h :set hlsearch!<cr>
nnoremap <leader>J J
nnoremap <leader>K K
nnoremap <leader>l :call ToggleConcealLevel()<cr>
nnoremap <leader>n :set invnumber<cr>
nnoremap <leader>o mzo<esc>`z
nnoremap <leader>O mzO<esc>`z
nnoremap <leader>q :q<cr>
nnoremap <leader>r :redraw!<cr>
nnoremap <leader>s s
nnoremap <leader>t :NERDTreeToggle<cr>
nnoremap <leader>v :e $MYVIMRC<cr>
nnoremap <leader>w :w<cr>
nnoremap <leader>W /\S\zs\s\+$<cr>:set hlsearch<cr>
nnoremap <leader>x :bd<cr>

" colors
syntax on
set t_Co=256
colorscheme seoul256


" c files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_c
	autocmd!
	au FileType c let b:file_dir = fnamemodify(expand('%:p:h'), ':p')
	au FileType c let b:makefile = b:file_dir . 'makefile'
	au FileType c let b:executable = expand('%:p:r')
	au FileType c nn <buffer> <localleader>c mz^i//<esc>`zl
	au FileType c nn <buffer> <localleader>u mz^xx`zh
	au FileType c nn ; :w<cr>:exe 'SlimeSend1 make'<cr>
	au FileType c nn <localleader>; :w<cr>:exe 'SlimeSend1' b:executable<cr>
augroup end


" cmake files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_cmake
	autocmd!
	au FileType cmake nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType cmake nn <buffer> <localleader>u mz^x`zh
augroup end


" conf files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_conf
	autocmd!
	au FileType conf nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType conf nn <buffer> <localleader>u mz^x`zh
augroup end


" cpp files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! Cout()
	" converts
	" a,b,cat
	" into
	" std::cout << "a,b,cat " << a << ", " << b << ", " << cat << std::endl;
	normal! gv"xy
	let selected_text = getreg("x")
	let word_list = split(selected_text, ',')
	let list_len = len(word_list)
	let cout_str = 'std::cout << "'
	for word in word_list[0:-2]
		let cout_str = cout_str . word . ','
	endfor
	let cout_str = cout_str . word_list[-1] . ' " << '
	for word in word_list[0:-2]
		let cout_str = cout_str . word . ' << ", " << '
	endfor
	let cout_str = cout_str . word_list[-1] . ' << std::endl;'
	call append(line('.'), ''.cout_str)
endfunction

function! CoutVector()
	" converts
	" myvec
	" into
	" std::cout << "myvec " << myvec[0] << ", " << myvec[1] << ", " << myvec[2] << std::endl;
	normal! gv"xy
	let vec_name = getreg("x")
	let cout_str =
		\ 'std::cout << "' . vec_name . ' " << ' .
		\ vec_name . '[0] << ", " << '  .
		\ vec_name . '[1] << ", " << ' .
		\ vec_name . '[2] << std::endl;'
	call append(line('.'), cout_str)
	normal! mzj==`z
endfunction

function! CoutMatrix()
	" converts
	" mymat
	" into
	" std::cout << "mymat " << mymat[0][0] << ", " << mymat[0][1] << ", " << mymat[0][2] << ", " << mymat[1][0] << ", " << mymat[1][1] << ", " << mymat[1][2] << ", " << mymat[2][0] << ", " << mymat[2][1] << ", " << mymat[2][2] << std::endl;
	normal! gv"xy
	let mat_name = getreg("x")
	let cout_str =
		\ 'std::cout << "' . mat_name . ' " << ' .
		\ mat_name . '[0][0] << ", " << '  .
		\ mat_name . '[0][1] << ", " << ' .
		\ mat_name . '[0][2] << ", " << ' .
		\ mat_name . '[1][0] << ", " << ' .
		\ mat_name . '[1][1] << ", " << ' .
		\ mat_name . '[1][2] << ", " << ' .
		\ mat_name . '[2][0] << ", " << ' .
		\ mat_name . '[2][1] << ", " << ' .
		\ mat_name . '[2][2] << std::endl;'
	call append(line('.'), ''.cout_str)
	normal! mzj==`z
endfunction

function! BuildRelease()
	let path = expand('%:p')[12:]
	"let path = '/workspaces/msw-components/src/file.cpp'[12:]
	let [text, start_pos, end_pos] = matchstrpos(path, '/')
	let path = path[:start_pos-1]
	execute 'SlimeSend1 build_release ' . path
endfunction

function! GetSelection()
    let old_reg = getreg("v")
    normal! gv"vy
    let raw_search = getreg("v")
    call setreg("v", old_reg)
	echo "hiiii"
    return substitute(escape(raw_search, '\/.*$^~[]'), "\n", '\\n', "g")
endfunction

augroup filetype_cpp
	autocmd!
	au FileType cpp let b:file_dir = fnamemodify(expand('%:p:h'), ':p')
	au FileType cpp let b:makefile = b:file_dir . 'makefile'
	au FileType cpp let b:executable = expand('%:p:r')
	au FileType cpp nn <buffer> <localleader>c mz^i//<esc>`zl
	au FileType cpp nn <buffer> <localleader>u mz^xx`zh
	au FileType cpp nn ; :w<cr>:exe 'SlimeSend1 make'<cr>
	au FileType cpp nn <localleader>; :w<cr>:exe 'SlimeSend1' b:executable<cr>
	au FileType cpp vn <localleader>c :call Cout()<cr>
	au FileType cpp vn <localleader>iv :call CoutVector()<cr>
	au FileType cpp vn <localleader>im :call CoutMatrix()<cr>
	au FileType cpp nn <localleader>br :call BuildRelease()<cr>
	" note: the mappings below overwrite some mappings i don't usually use
	au FileType cpp nn gc :YcmCompleter GoToDeclaration<cr>
	au FileType cpp nn gf :YcmCompleter GoToDefinition<cr>
	au FileType cpp nn gr :YcmCompleter GoToReferences<cr>
augroup end


" desktop files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_desktop
	autocmd!
	au FileType desktop nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType desktop nn <buffer> <localleader>u mz^x`zh
augroup end


" dockerfile files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_dockerfile
	autocmd!
	au FileType dockerfile nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType dockerfile nn <buffer> <localleader>u mz^x`zh
augroup end


" html
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_html
	autocmd!
	au FileType html nn <buffer> <localleader>c mz^i<!--<esc>$a--><esc>`zl
	au FileType html nn <buffer> <localleader>u mz^4x$xxx`zh
	au FileType html setlocal indentexpr="" # turn off annoying indentation
augroup end


" makefiles
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_make
	autocmd!
	au FileType make nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType make nn <buffer> <localleader>u mz^x`zh
	au FileType make nn ; :w<cr>:exe 'SlimeSend1 make'<cr>
augroup end


" py files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_python
	autocmd!

	" run the file or part of it
	au FileType python nn ; :w<cr>:exe 'SlimeSend1 python' expand('%:p')<cr>
	au FileType python nn <localleader>p;
		\ :exe 'SlimeSend1 python' expand('%:p')<cr>
	au FileType python nn <localleader>pp :exe 'SlimeSend1 python'<cr>
	au FileType python nn <localleader>pt
		\ :exe 'SlimeSend1 time python' expand('%:p')<cr>
	au FileType python nn <localleader>pe :exe 'SlimeSend1 exit()'<cr>
	au FileType python nn <localleader>pi
		\ :exe 'SlimeSend1 python -i' expand('%:p')<cr>
	au FileType python nn <localleader>ps
		\ :exe 'SlimeSend1 python'<cr>:exe '1,' . line('$') . 'SlimeSend'<cr>
	au FileType python vm <localleader>ps <Plug>SlimeRegionSend
	au FileType python nn <localleader>i
		\ :exe 'SlimeSend1 ipython' expand('%:p')<cr>
	au FileType python nn <localleader>pb
		\ :exe 'SlimeSend1 blender --python ' . expand('%:p')<cr>
	au FileType python nn <localleader>b :exe
		\ 'SlimeSend1 blender --background --python ' . expand('%:p')<cr><cr>

	" comments
	au FileType python nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType python vn <buffer> <localleader>c mz$<c-v>0<s-i>#<esc>`zl
	au FileType python nn <buffer> <localleader>u mz^x`zh
	au FileType python nn <buffer> <localleader>kp mz{j^y0kpa'''<esc>}pa'''<esc>`z
	au FileType python nn <buffer> <localleader>ku mz?'''<cr>0d$/'''<cr>0d$`z

	" various
	au FileType python nn <localleader>C :call ToggleColorColumn(79)<cr>
	au FileType python nn <buffer> <localleader>pd mz^oimport pdb; pdb.set_trace()<esc>`z
augroup end


" sh files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_shell
	autocmd!
	au FileType sh,zsh nn ; :w<cr>:execute 'SlimeSend1 ' expand('%:p')<cr>
	au FileType sh,zsh nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType sh,zsh nn <buffer> <localleader>u mz^x`zh
	au FileType sh,zsh vm <localleader>ps <Plug>SlimeRegionSend
	au FileType sh,zsh nn <buffer> <localleader>kp mz{j^y0kpa:'<esc>}pa'<esc>`z
	au FileType sh,zsh nn <buffer> <localleader>ku mz?:'<cr>0d$/\n'\n<cr>j0d$`z
augroup end


" tex files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:tex_flavor = 'latex'
function! TexSetDirsAndFiles(tex_main_file)
	" in case this is a multi-file project
	" e.g. /home/trevor/project/file1.tex
	let b:tex_current_file = expand('%:p')
	" e.g. /home/trevor/project/root.tex
	let b:tex_main_file = fnamemodify(a:tex_main_file, ':p')
	" e.g. /home/trevor/project
	let b:tex_main_dir = fnamemodify(a:tex_main_file, ':p:h')
	" e.g. /home/trevor/project/root
	let b:tex_main_root = fnamemodify(a:tex_main_file, ':p:r')
	" e.g. /home/trevor/project/root.pdf
	let b:tex_main_pdf = b:tex_main_root . '.pdf'
endfunction

function! StartServer()
	if empty(v:servername) && exists('*remote_startserver')
		call remote_startserver('VIM')
	endif
	call system('xdotool getactivewindow > /tmp/vim_wid.txt')
endfunction

function! TexCompile(cmd)
	if a:cmd == 'latexmk'
		let tex_cmd =
			\ 'cd ' . b:tex_main_dir . '; ' .
			\ 'latexmk -jobname=temp -pdf -interaction=nonstopmode -pdflatex="pdflatex -halt-on-error -synctex=1" -cd ' .
			\ b:tex_main_file .
			\ ' && cp temp.pdf ' . expand(b:tex_main_pdf) .
			\ ' && cp temp.synctex.gz ' . expand(b:tex_main_root) . '.synctex.gz' .
			\ " || vim --remote-send \":echo \' ☠️ COMPILATION FAILED ☠️ \'<CR>\"" .
			\ '; cd -'
		"call system('echo "' . b:tex_main_pdf . '" > /home/trevor/Downloads/dump.txt')

	elseif a:cmd == 'pdflatex'
		let tex_cmd =
			\ 'cd ' . b:tex_main_dir . '; ' .
			\ 'pdflatex -halt-on-error -synctex=1 ' .
			\	fnamemodify(b:tex_main_file, ':t') . ' || ' .
			\ 'if [ -f ' . expand(b:tex_main_pdf, ':t') . ' ]; then ' .
			\ '> ' . expand(b:tex_main_pdf) . '; fi; ' .
			\ 'cd -'

	" to manually change a:cmd, do
	elseif a:cmd == 'lualatex'
		let tex_cmd =
			\ 'cd ' . b:tex_main_dir . '; ' .
			\ 'lualatex -jobname=temp -pdf -interaction=nonstopmode -lualatex="lualatex -halt-on-error" -cd ' .
			\ b:tex_main_file .
			\ ' && cp temp.pdf ' . expand(b:tex_main_pdf) .
			\ " || vim --remote-send \":echo \' ☠️ COMPILATION FAILED ☠️ \'<CR>\"" .
			\ '; cd -'

	elseif a:cmd == 'xelatex'
		let tex_cmd =
			\ 'cd ' . b:tex_main_dir . '; ' .
			\ 'xelatex -jobname=temp -pdf -interaction=nonstopmode -lualatex="lualatex -halt-on-error" -cd ' .
			\ b:tex_main_file .
			\ ' && cp temp.pdf ' . expand(b:tex_main_pdf) .
			\ " || vim --remote-send \":echo \' ☠️ COMPILATION FAILED ☠️ \'<CR>\"" .
			\ '; cd -'
	endif

	execute 'SlimeSend1 ' . expand(tex_cmd)
	redraw
endfunction

function! OkularScroll(down_or_up)
	call system('okular_down_up.sh ' . a:down_or_up)
endfunction

function! OkularFind()
	call StartServer()
	let okular_find_job = job_start([
		\ '/bin/bash', '-c',
		\ 'okular_find.sh ' . b:tex_main_pdf . ' ' .
		\ line('.') . ' ' . b:tex_current_file])
	redraw
endfunction

function! TexClean()
	call delete(b:tex_main_root . '.aux')
	call delete(b:tex_main_root . '.bbl')
	call delete(b:tex_main_root . '.blg')
	call delete(b:tex_main_root . '.fdb_latexmk')
	call delete(b:tex_main_root . '.fls')
	call delete(b:tex_main_root . '.log')
	call delete(b:tex_main_root . '.synctex')
	call delete(b:tex_main_root . '.synctex.gz')
endfunction

augroup filetype_tex
	autocmd!
	au FileType tex if getline(1)=='%lualatex' | let b:compile_command = 'lualatex' | else | let b:compile_command = 'latexmk' | endif
	au FileType tex nn j gj
	au FileType tex nn k gk
	au FileType tex vn j gj
	au FileType tex vn k gk
	au FileType tex nn <buffer> s :w<cr>:call TexCompile(b:compile_command)<cr>
	au FileType tex nn <buffer> ; :w<cr>:call TexCompile(b:compile_command)<cr>
	au FileType tex nn <localleader>d :call TexClean()<cr>
	au FileType tex nn <localleader>f :call OkularFind()<cr>
	au FileType tex nn <localleader>j :call OkularScroll('down')<cr>
	au FileType tex nn <localleader>k :call OkularScroll('up')<cr>
	au FileType tex nn <localleader>p :let okular_job =
		\ job_start('okular ' . expand('%:r') . '.pdf')<cr>
	au FileType tex nn <buffer> <localleader>kp
		\ mz{j^y0kpa\iffalse<esc>}pa\fi<esc>`z
	au FileType tex nn <buffer> <localleader>ku
		\ mz?\\iffalse<cr>0d$/\\fi<cr>0d$`z
	au FileType tex nn <buffer> <localleader>c mz^i%<esc>`zl
	au FileType tex nn <buffer> <localleader>u mz^x`zh
	au FileType tex nn <localleader>ia i\begin{align}<cr><cr>\end{align}<Up>
	au FileType tex nn <localleader>iA i\begin{align*}<cr><cr>\end{align*}<Up>
	au FileType tex nn <localleader>ib
		\ i\begin{bmatrix}   \end{bmatrix}<esc>? <cr>h
	au FileType tex nn <localleader>ic i\begin{cases}<cr><cr>\end{cases}<Up>
	au FileType tex nn <localleader>ie
		\ i\begin{equation}<cr><cr>\end{equation}<Up>
	au FileType tex nn <localleader>iE
		\ i\begin{equation*}<cr><cr>\end{equation*}<Up>
	au FileType tex nn <localleader>if
		\ i\begin{figure}[ht]<cr><cr>\end{figure}[ht]<Up>
	au FileType tex setlocal spell spelllang=en_us
	au FileType tex filetype indent off
	au BufEnter *.tex if !exists('b:tex_main_file') |
		\ call TexSetDirsAndFiles(expand('%:p')) | endif
augroup end


" tmux files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_tmux
	autocmd!
	au FileType tmux nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType tmux nn <buffer> <localleader>u mz^x`zh
augroup end


" txt files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_txt
	autocmd!
	au FileType text nn j gj
	au FileType text nn k gk
	au FileType text vn j gj
	au FileType text vn k gk
	autocmd FileType text setlocal spell
	autocmd FileType text nnoremap <leader>sp :call ToggleSpellCheck()<cr>
augroup end


" vim files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_vim
	autocmd!
	autocmd FileType vim nnoremap ; :w<cr>:source $MYVIMRC<cr>
	autocmd FileType vim nnoremap <buffer> <localleader>c mz^i"<esc>`zl
	autocmd FileType vim nnoremap <buffer> <localleader>u mz^x`zh
augroup end


" yaml files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_yaml
	autocmd!
	au FileType yaml nn <buffer> <localleader>c mz^i#<esc>`zl
	au FileType yaml nn <buffer> <localleader>u mz^x`zh
augroup end


" plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-slime
let g:slime_target = 'tmux'
let g:slime_default_config =
	\ {'socket_name': 'default', 'target_pane': 'output:0.0'}
let g:slime_dont_ask_default = 1

" NERDTree
let NERDTreeShowBookmarks=1
let NERDTreeQuitOnOpen=1

" YouCompleteMe
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_clangd_binary_path='/usr/bin/clangd'


" show names of Greek letters as symbols
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! GreekSymbols()
	syntax cluster Greek_symbols contains=GS_a,GS_b,GS_c,GS_d,GS_ep,GS_et,G_g,G_l,G_m,G_n,G_k,G_o,G_ph,G_ps,G_pi,G_r,G_s,G_ta,G_th,G_x,G_z
	syntax match G_a "alpha" conceal cchar=α
	syntax match G_b "beta" conceal cchar=β
	syntax match G_c "chi" conceal cchar=χ
	syntax match G_d "delta" conceal cchar=δ
	syntax match G_ep "epsilon" conceal cchar=ε
	syntax match G_et "eta" conceal cchar=η
	syntax match G_g "gamma" conceal cchar=γ
	syntax match G_l "lambda" conceal cchar=λ
	syntax match G_m "mu" conceal cchar=μ
	syntax match G_n "nu" conceal cchar=ν
	syntax match G_k "kappa" conceal cchar=κ
	syntax match G_o "omega" conceal cchar=ω
	syntax match G_ph "phi" conceal cchar=φ
	syntax match G_ps "psi" conceal cchar=ψ
	syntax match G_pi "pi" conceal cchar=π
	syntax match G_r "rho" conceal cchar=ρ
	syntax match G_s "sigma" conceal cchar=σ
	syntax match G_ta "tau" conceal cchar=τ
	syntax match G_th "theta" conceal cchar=θ
	syntax match G_x "xi" conceal cchar=ξ
	syntax match G_z "zeta" conceal cchar=ζ
endfunction

nnoremap <leader>g :call GreekSymbols()<cr>


" nerdtree - custom command to execute a file
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! NerdTreeExecute()
	let l:filename=g:NERDTreeFileNode.GetSelected().path.str()
	let l:extension=fnamemodify(filename,':e')
	if l:extension=='py'
		execute 'SlimeSend1 python ' . l:filename
	elseif l:exension=='sh'
		execute 'SlimeSend1 ' . l.filename
	endif
endfunction

autocmd VimEnter * call NERDTreeAddMenuItem({'text': 'e(x)ecute', 'shortcut': 'x', 'callback': 'NerdTreeExecute'})

nnoremap <leader-F1> :pwd<cr>
nnoremap <leader-F2> :pwd<cr>
nnoremap <leader><F3> :pwd<cr>
nnoremap <leader><F4> :pwd<cr>
nnoremap <leader><F5> :pwd<cr>
nnoremap <F1> :pwd<cr>
nnoremap <F2> :pwd<cr>
nnoremap <F3> :pwd<cr>
nnoremap <F4> :pwd<cr>
nnoremap <f5> :pwd<cr>
nnoremap <C-1> jj
nnoremap <C-2> jj
nnoremap <C-F1> jj
nnoremap <C-F2> jj
nnoremap <s-left> jj
nnoremap <s-right> jj
"
nnoremap <leader>1 :call system('tmux_filenames.sh 1')<cr>
nnoremap <leader>2 :call system('tmux_filenames.sh 2')<cr>
nnoremap <leader>3 :call system('tmux_filenames.sh 3')<cr>
nnoremap <leader>4 jj
