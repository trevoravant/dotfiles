#
# ~/.bash_profile
#
if [ "$HOSTNAME" = trevorsdesktop ]; then
	# from Arch Forums
	#gnome-session --session=gnome-wayland
	# from Arch Wiki
	if [[ -z $DISPLAY && $(tty) == /dev/tty1 && $XDG_SESSION_TYPE == tty ]]; then
		MOZ_ENABLE_WAYLAND=1 QT_QPA_PLATFORM=wayland XDG_SESSION_TYPE=wayland exec dbus-run-session gnome-session
	fi
elif [ "$HOSTNAME" = trevorslaptop ]; then
	[[ -f ~/.bashrc ]] && . ~/.bashrc
	# new startx automatically, from Arch forums post about
	# https://bbs.archlinux.org/viewtopic.php?id=259149
	[[ ! $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
	# gurobi stuff
	export GUROBI_HOME="/home/trevor/Downloads/gurobi902/linux64"
	export GRB_LICENSE_FILE="/home/trevor/Downloads/gurobi902/gurobi.lic"
	export PATH="${PATH}:${GUROBI_HOME}/bin"
	export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
fi

# add to path
PATH="$HOME/code/shell_scripts:$HOME/.local/bin:$PATH"
